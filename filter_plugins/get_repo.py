import os.path
import re


def get_rh_mirorlist(url, distro, arch, version):
    distro = distro.lower()
    arch = arch.lower()

    if distro == "fedora":
        if version == "rawhide":
            return url % "rawhide"
        else:
            return url % "fedora-$releasever"
    elif distro in ("centos", "rhel"):
        p = re.match(r"(\d+)", str(version))
        if not p:
            raise ValueError("version needs to start with a number")
        version_number = int(p.group(1))

        if version_number >= 8:
            return url % "BaseOS"
        else:
            return url % "os"
    else:
        return


def get_repo(mirror, distro, arch, version):
    # TODO check the naming of the various arch
    # TODO automate getting a default mirror ?
    distro = distro.lower()
    arch = arch.lower()
    path = ""

    if distro in ("debian", "ubuntu") and arch == "x86_64":
        arch = "amd64"

    # beware, os.path.join will detect if path start by a '/'
    # and likely behave unexpectedly
    # os.path.join('/foo', '/bar') => '/bar'
    if distro == "fedora":
        t = "releases"
        if version == "rawhide":
            t = "development"
        path = "%s/linux/%s/%s/Server/%s/os" % (distro, t, version, arch)
    if distro in ("centos", "rhel"):
        p = re.match(r"(\d+)", str(version))
        if not p:
            raise ValueError("version needs to start with a number")
        version_number = int(p.group(1))

        if version_number < 8:
            path = "%s/%s/os/%s" % (distro, version, arch)
        else:
            path = "%s/%s/BaseOS/%s/os" % (distro, version, arch)
    if distro in ("debian", "ubuntu"):
        path = "%s/dists/%s/main/installer-%s" % (distro, version, arch)
    return os.path.join(mirror, path)


class FilterModule(object):
    def filters(self):
        return {
            "get_repo": get_repo,
            "get_rh_mirorlist": get_rh_mirorlist,
        }
