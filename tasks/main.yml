---
- name: Include OS-specific variables
  include_vars: "{{ item }}"
  with_first_found:
  - "{{ ansible_os_family }}_{{ ansible_distribution_major_version }}.yml"
  - "default.yml"

- name: Convert vm_name to name
  set_fact:
    name: "{{ vm_name }}"
  when: vm_name is defined

- name: Install guestfs python bindings
  package:
    name: "{{ (ansible_python['version']['major'] == 3) | ternary('python3-libguestfs', 'python-libguestfs') }}"
    state: present

- name: "Warn about deprecated 'disk_size' parameter"
  debug:
    msg: disk_size is deprecated and replaced by system_disk_size, please update
  when: disk_size is defined

# should fail somewhere around 1st of July 2017 (ie 6 month after this patch was submitted)
- name: "Fail if deprecated 'disk_size' parameter is still in use"
  fail:
    msg: disk_size is now deprecated and failling, please update
  when: disk_size is defined and ansible_date_time['epoch'] |int > 1498924000

- name: "Define 'system_disk_size' using deprecated 'disk_size'"
  set_fact:
    system_disk_size: "{{ disk_size }}"
  when: disk_size is defined and system_disk_size is not defined

# we use 5G for / and 0.5G for /boot on RHEL kickstart
- name: "Fail when system disk size is too small"
  fail:
    msg: Disk is too small, we need at least 6G for the system
  when: system_disk_size|int < 6

- name: Ensure the lv for {{ name }} system disk is made
  lvol:
    lv: "{{ name }}"
    vg: "{{ volgroup }}"
    size: "{{ system_disk_size |default(disk_size) }}G"
    state: present

- name: Ensure the lv for {{ name }} data disk is made
  lvol:
    lv: "{{ name }}_data"
    vg: "{{ volgroup }}"
    size: "{{ data_disk_size }}G"
    state: present
  when: data_disk_size|int > 0

- name: Check that the VM is not running yet
  virt:
    command: list_vms
  check_mode: no
  register: result

- name: Set the maximum number of cpus
  set_fact:
    num_cpus_max: "{{ num_cpus * 2 }}"
  when: num_cpus_max is not defined or num_cpus_max == 0

- name: Set maximum memory
  set_fact:
    mem_size_max: "{{ mem_size * 2 }}"
  when: mem_size_max is not defined or mem_size_max == 0

- name: Check that memory is not too low
  fail:
    msg: "Memory is too low"
  when: mem_size < 512

- name: Check that max CPU and CPU are ok
  fail:
    msg: "Too much cpus requested for the max cpus settings"
  when: num_cpus > num_cpus_max|int

- name: Check that max mem and mem are ok
  fail:
    msg: "Too much memory requested for the max memory settings"
  when: mem_size > mem_size_max|int

- name: "Warn about deprecated 'network.bootproto' parameter"
  debug:
    msg: network.bootproto is deprecated and autodetected, please update
  when: network.bootproto is defined

# should fail somewhere around mid September 2018 (ie 6 month after this patch was merged)
- name: "Fail if deprecated 'network.bootproto' parameter is still in use"
  fail:
    msg: network.bootproto is deprecated and failling, please update
  when: network.bootproto is defined and ansible_date_time['epoch'] |int > 1537135200


- name: Default to network.bootproto for compat
  set_fact:
    bootproto: "{{ network.bootproto }}"
  when: network.bootproto is defined

- name: Be smart and use static if a IP is given
  set_fact:
    bootproto: "static"
  when: network.ip is defined

- name: Verify static network configuration
  block:
    - name: Check that the prefix is correct
      fail:
        msg: "Incorrect network prefix"
      # use /31 mean that you have only 2 ip v4, so 1 for network, and 1 for broadcast
      when: network.ip is defined and (network.ip | ipv4('prefix') | int  >= 31)
  when: bootproto == 'static'

- name: Set default DNS servers
  set_fact:
    nameservers: "{{ network.nameservers }}"
  when: network.nameservers is defined

- name: Set target distro family to Debian
  set_fact:
    distro_family: "Debian"
  when: distribution == 'Ubuntu' or distribution == 'Debian'

- name: Set target distro family to Redhat
  set_fact:
    distro_family: "Redhat"
  when: distribution == 'Fedora' or distribution == 'Centos' or distribution == 'RHEL'

# if repo_url is not set here it is calculated using default_mirrors and get_repo()
- name: "Check for up-to-date repo URL with mirror list"
  block:
    - name: "Download mirrorlist"
      uri:
        url: "{{ rh_repo_mirrorlist[distribution] | get_rh_mirorlist(distribution, arch, version) | replace('$releasever', version) | replace('$basearch', arch) }}"
        return_content: yes
      register: repo_mirrorlist
    - name: "Use repo from mirror list"
      set_fact:
        # mirrorlists might have comments (mirrors.fedoraproject.org does)
        repo_url: "{{ (repo_mirrorlist.content | trim).split('\n') | reject('search', '^#') | list | random }}"
      when: repo_mirrorlist.status == 200
  when: rh_repo_mirrorlist[distribution] is defined

- name: Generate kickstart
  include_tasks: kickstart.yml
  when: distro_family == 'Redhat'

- name: Generate preseed
  include_tasks: preseed.yml
  when: distro_family == 'Debian'

- name: "Create VM for {{ name }}"
  block:
    - name: "Allow reboot at creation time"
      set_fact:
        _vm_reboot_ok: True

    # in case we reinstall
    - name: "Remove the ssh keys from past hosts (name)"
      known_hosts:
        name: "{{ name }}"
        state: absent
      delegate_to: 127.0.0.1
    - name: "Remove the ssh keys from past hosts (ip)"
      known_hosts:
        name: "{{ hostvars[name].ansible_host }}"
        state: absent
      delegate_to: 127.0.0.1
      when: hostvars[name] is defined and hostvars[name].ansible_host is defined

    - name: Run the virt-install for {{ name }}
      command: "{{ virt_install_command }}"

    - name: Wait for the install of {{ name }} to finish
      virt:
        command: status
        name: "{{ name }}"
      register: vmstatus
      until: vmstatus.status == 'shutdown'
      retries: 1500
      delay: 10

    - name: Start the VM for {{ name }}
      virt:
        command: start
        name: "{{ name }}"

    # keys are generated on first boot, so we need to fetch them after
    - name: Get the ssh keys from the VM
      vm_ssh_pub_keys_facts:
        name: "{{ name }}"
      register: keys
      until: '"key_files" in keys and (keys["key_files"]|length) > 0'
      retries: 10
      delay: 10

    - name: Add the ssh keys to the current host
      known_hosts:
        key: "{{ name + ' ' + keys['key_files'][item] }}"
        name: "{{ name }}"
      with_items: "{{ keys['key_files'] }}"
      delegate_to: 127.0.0.1

    - name: "Install Python"
      raw: test -e /usr/bin/python3 || (test -e /usr/bin/apt && (apt -qqy update && apt install -qqy python3-minimal) || (dnf -qy install python3))
      register: output
      changed_when: output.stdout != ""
      delegate_to: "{{ vm_name }}"
      # we cannot use facts yet, so relying on inventory vars
      when: ansible_python_interpreter is defined and 'python3' in ansible_python_interpreter

    - name: "Add main interface mapping"
      include_tasks: bridge.yml
      vars:
        iface: "{{ bridge_iface }}"
      when: bridge_iface is defined

    - name: "Rename main interface in configuration"
      include_tasks: main_iface_rename_config.yml
      # no need to rename if using eth0 because we already force this name at install time
      when: bridge_iface is defined and bridge_iface != "eth0"

  when: name not in result.list_vms

- name: Clean the variables so they do not leak
  set_fact:
    num_cpus_max: 0
    mem_size_max: 0
