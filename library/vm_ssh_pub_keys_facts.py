#!/usr/bin/python
# coding: utf-8 -*-

# (c) 2017, Michael Scherer <mscherer@redhat.com>
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software.  If not, see <http://www.gnu.org/licenses/>.

ANSIBLE_METADATA = {'metadata_version': '1.0',
                    'status': ['preview'],
                    'supported_by': 'community'}

DOCUMENTATION = '''
---
module: vm_ssh_pub_keys_facts
short_description: Get the fingerprint of ssh pub keys
version_added:
options:
  name:
    required: True
    description: Name of the vm to examine
  type:
    required: False
    description: Type of the VM ('domain' or 'image'), default to 'domain'
'''

EXAMPLES = '''
# keys are generated on first boot of a VM
# so we need to fetch them after and make sure they
# are commited on disk
- name: Get the ssh keys from the VM
  vm_ssh_pub_keys_facts:
    name: "{{ name }}"
  register: keys
  until: '"key_files" in keys and (keys["key_files"]|length) > 0'
  retries: 10
  delay: 10
'''

RETURN = '''
key_files:
  description: A dictionnary with filename as keys and key content as values
  returned: success
  type: complex
  contains:
    filename:
      description: The filename of a key
      returned: always
      type: complex
      contains:
        fingerprint:
          result: always
          type: string
          sample: ssh-rsa AAABCEDF
'''
from ansible.module_utils.basic import AnsibleModule

try:
    import guestfs
    HAVE_GUESTFS = True
except ImportError:
    HAVE_GUESTFS = False


def main():

    module = AnsibleModule(
        argument_spec=dict(
            name=dict(required=True),
            type=dict(choices=['domain', 'image'], default='domain'),
        ),
        supports_check_mode=False
    )

    name = module.params['name']
    results = {}

    if not HAVE_GUESTFS:
        module.fail_json(msg="This module requires guestfs python bindings")

    g = guestfs.GuestFS(python_return_dict=True)
    g.set_backend('direct')
    if module.params['type'] == 'domain':
        g.add_domain(name, readonly=1)
    else:
        g.add_drive_opts(name, readonly=1)
    g.launch()

    roots = g.inspect_os()
    # safe to assume, since we have 1 single
    # sysem installed with guest-virt-install
    root = roots[0]

    # cut and paste from guestfs man pages
    mps = g.inspect_get_mountpoints(root)

    for device in sorted(mps.keys(), key=lambda x: len(x)):
        try:
            g.mount_ro(mps[device], device)
        except RuntimeError as msg:
            print("%s (ignored)" % msg)

    ssh_dir = '/etc/ssh'
    for f in g.ls(ssh_dir):
        if f.endswith('.pub'):
            k = g.cat(ssh_dir + '/' + f)
            if k.strip() != "":
                results[f] = k.strip()

    module.exit_json(changed=False, key_files=results)

main()
