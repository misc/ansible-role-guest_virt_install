# {{ ansible_managed }}
{% set version_number = version | string | regex_search('^([0-9]+)', '\\1') | first | int %}

text

lang {{ lang }}.UTF-8
keyboard {{ keyboard }}

network --device=eth0 --onboot=yes --bootproto={{ bootproto }}{% if bootproto == 'static' %} --ip={{ network.ip | ipaddr('address') }} --netmask={{ network.ip | ipaddr('netmask') }}{% for n in nameservers %} --nameserver={{ n }}{% endfor %} --gateway={{ network.gateway }}{% endif %} --noipv6 --hostname={{ name }}

{% if distribution in rh_repo_mirrorlist %}
url --mirrorlist={{ rh_repo_mirrorlist[distribution] | get_rh_mirorlist(distribution, arch, version) }}
{% endif %}

timezone --utc {{ timezone }}
rootpw --iscrypted {{ root_pw |password_hash('sha512', hash) }}
{% if sshkey is defined %}
{% if distribution == 'Fedora' %}
sshkey --username=root "{{ sshkey }}"
{% endif %}
{% endif %}
{% if (distribution == 'Fedora' and (version == 'rawhide' or version_number >= 29)) or ((distribution == 'Centos' or distribution == 'RHEL') and version_number >= 8) %}
# we can use authselect instead of authconfig but the default seems fine
{% else %}
authconfig --enableshadow --passalgo=sha512 --enablefingerprint
{% endif %}

bootloader --location=mbr --driveorder=vda

zerombr
clearpart --all --drive=vda
part /boot --fstype={{ filesystem }} --size=500 --asprimary --ondisk=vda
part pv.01 --grow --size=5000 --ondisk=vda
volgroup vg_root_{{ name }} pv.01
logvol / --vgname=vg_root_{{ name }} --fstype={{ filesystem }} --grow --size=5000 --maxsize=8000 --name=root

services --enabled=sshd
firewall --service=ssh

reboot

%packages

openssh-server
{% if (distribution == 'Fedora' and (version == 'rawhide' or version_number >= 29)) or ((distribution == 'Centos' or distribution == 'RHEL') and version_number >= 8) %}
# Ansible is now able to detect the install Python version
#python3
# not installed by default in EL9+ and F35+ (at least)
policycoreutils-python-utils
{% else %}
python
{% endif %}

{% if extra_packages is defined %}
{% for p in extra_packages %}
{{ p }}
{% endfor %}
{% endif %}
%end

%post --log=/root/ansible_post.log
{% if sshkey is defined %}
{% if distribution == 'Centos' or distribution == 'RHEL' %}
mkdir -p /root/.ssh/
chmod 700 /root/.ssh/
echo "{{ sshkey }}" >> /root/.ssh/authorized_keys
restorecon -Rv /root/.ssh/
{% endif %}
config="/etc/ssh/sshd_config"
if grep -q '^PermitRootLogin ' $config; then
    sed 's/^PermitRootLogin .*/PermitRootLogin without-password/' -i $config
else
    echo "PermitRootLogin without-password" >> $config
fi;

{% endif %}

{% if postinstall is defined %}
{{ postinstall }}
{% endif %}
%end
